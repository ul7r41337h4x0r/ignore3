/*
    ignore3 Copyright (C) 2018  scott snyder

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"flag"
	"fmt"
	"github.com/boombuler/barcode/pdf417"
	"golang.org/x/crypto/twofish"
	"image/png"
	"io"
	"io/ioutil"
	"math/big"
	"os"
	"time"
)

const (
	delay   = 2
	version = "0.01a"
)

var (
	outname      string
	pemfile      string
	file         string
	list         [33]string
	passestodo   int
	operationarg string
	inputarg     string
	outputarg    string
	keyarg       string
	method       string
	verbose      *bool
)

func slowly(inputtext string, delay int) {
	passes := len(inputtext)
	for i := 0; i <= passes; i++ {
		fmt.Printf("%s", string(inputtext[i]))
		time.Sleep(time.Duration(delay) * time.Millisecond)
		if i == (passes - 1) {
			break
		}
	}
}

func keygen(keysize int, lowerch, upperch, numbers, symbols, crazyrn bool) string {
	var lower = "abcdefghijklmnopqrstuvwxyz"
	var upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	var number = "12345678901234567890"
	var symb = "!@#$^&*()_-+={}[]:;'`~,./?"
	var crazy = "¡¢£¥¦§«»¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ"
	var bldstr string
	var output = ""
	if lowerch == true {
		output = output + lower
	}
	if upperch == true {
		output = output + upper
	}
	if numbers == true {
		output = output + number
	}
	if symbols == true {
		output = output + symb
	}
	if crazyrn == true {
		output = output + crazy
	}
	for {
		output = output + output
		if len(output) >= 1000000 {
			break
		}
	}
	intlen := int64(len(output))
	for i := 1; i <= keysize; i++ {
		res, _ := rand.Int(rand.Reader, big.NewInt(intlen))
		r := output[res.Int64()]
		s := fmt.Sprintf("%c", r)
		bldstr = bldstr + s
	}
	return bldstr
}

func savebarcode() {
	infile, _ := ioutil.ReadFile("key.pem")
	decryptkey := string(infile)
	bc, _ := pdf417.Encode(decryptkey, 2)
	file, _ := os.Create("key.png")
	defer file.Close()
	png.Encode(file, bc)
}

func keybuild(keystr string) {
	var pt1 = 0
	var pt2 = 32
	data := keystr
	for i := 1; i <= 32; i++ {
		list[i] = data[pt1:pt2]
		pt1 = pt1 + 32
		pt2 = pt2 + 32
		if i == 32 {
			break
		}
	}
}

func encryptTF(plainstring, keystring string) string {
	plaintext := []byte(plainstring)
	key := []byte(keystring)
	block, err := twofish.NewCipher(key)
	if err != nil {
		panic(err)
	}
	ciphertext := make([]byte, twofish.BlockSize+len(plaintext))
	iv := ciphertext[:twofish.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		panic(err)
	}
	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[twofish.BlockSize:], plaintext)
	return string(ciphertext)
}

func decryptTF(cipherstring string, keystring string) string {
	ciphertext := []byte(cipherstring)
	key := []byte(keystring)
	block, err := twofish.NewCipher(key)
	if err != nil {
		panic(err)
	}
	iv := ciphertext[:twofish.BlockSize]
	ciphertext = ciphertext[twofish.BlockSize:]
	stream := cipher.NewCFBDecrypter(block, iv)
	stream.XORKeyStream(ciphertext, ciphertext)
	return string(ciphertext)
}

func encryptAES(plainstring, keystring string) string {
	plaintext := []byte(plainstring)
	key := []byte(keystring)
	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}
	ciphertext := make([]byte, aes.BlockSize+len(plaintext))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		panic(err)
	}
	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], plaintext)
	return string(ciphertext)
}

func decryptAES(cipherstring string, keystring string) string {
	ciphertext := []byte(cipherstring)
	key := []byte(keystring)
	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}
	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]
	stream := cipher.NewCFBDecrypter(block, iv)
	stream.XORKeyStream(ciphertext, ciphertext)
	return string(ciphertext)
}

func defile(filetodecrypt, method string) {
	if len(pemfile) >= 3 {
		infile, _ := ioutil.ReadFile(pemfile)
		decryptkey := string(infile)
		keybuild(decryptkey)
	}
	d, _ := ioutil.ReadFile(filetodecrypt)
	d1 := string(d)
	if method == "both" {
		if *verbose {
			fmt.Println("AES/Twofish decrypt")
		}
		for q := 1; q <= passestodo; q++ {
			ct := 32
			dt := 1
			if *verbose {
				fmt.Printf("\033[spass %d/%d \033[u", q, passestodo)
			}
			for i := 0; i <= 32; i++ {
				d1 = decryptAES(d1, list[ct])
				d1 = decryptTF(d1, list[dt])
				dt++
				ct--
				if ct == 0 {
					break
				}
			}
			if q == passestodo {
				break
			}
		}
	}
	if method == "aes" {
		if *verbose {
			fmt.Println("AES decrypt")
		}
		for q := 1; q <= passestodo; q++ {
			ct := 32
			if *verbose {
				fmt.Printf("\033[spass %d/%d \033[u", q, passestodo)
			}
			for i := 0; i <= 32; i++ {
				d1 = decryptAES(d1, list[ct])
				ct--
				if ct == 0 {
					break
				}
			}
			if q == passestodo {
				break
			}
		}
	}
	if method == "twofish" {
		if *verbose {
			fmt.Println("Twofish decrypt")
		}
		for q := 1; q <= passestodo; q++ {
			ct := 32
			if *verbose {
				fmt.Printf("\033[spass %d/%d \033[u", q, passestodo)
			}
			for i := 0; i <= 32; i++ {
				d1 = decryptTF(d1, list[ct])
				ct--
				if ct == 0 {
					break
				}
			}
			if q == passestodo {
				break
			}
		}
	}
	ioutil.WriteFile(outname, []byte(d1), 0664)
}

func enfile(filestr, method string) {
	if len(pemfile) >= 3 {
		infile, _ := ioutil.ReadFile(pemfile)
		userkeystr := string(infile)
		keybuild(userkeystr)
	}
	e, _ := ioutil.ReadFile(filestr)
	e1 := string(e)
	if method == "both" {
		if *verbose {
			fmt.Println("AES/Twofish encrypt")
		}
		for q := 1; q <= passestodo; q++ {
			if *verbose {
				fmt.Printf("\033[spass %d/%d \033[u", q, passestodo)
			}
			dt := 32
			for i := 1; i <= 32; i++ {
				e1 = encryptTF(e1, list[dt])
				e1 = encryptAES(e1, list[i])
				dt--
				if i == 32 {
					break
				}
			}
			if q == passestodo {
				break
			}
		}
	}
	if method == "aes" {
		if *verbose {
			fmt.Println("AES encrypt")
		}
		for q := 1; q <= passestodo; q++ {
			if *verbose {
				fmt.Printf("\033[spass %d/%d \033[u", q, passestodo)
			}
			for i := 1; i <= 32; i++ {
				e1 = encryptAES(e1, list[i])
				if i == 32 {
					break
				}
			}
			if q == passestodo {
				break
			}
		}
	}
	if method == "twofish" {
		if *verbose {
			fmt.Println("Twofish encrypt")
		}
		for q := 1; q <= passestodo; q++ {
			if *verbose {
				fmt.Printf("\033[spass %d/%d \033[u", q, passestodo)
			}
			for i := 1; i <= 32; i++ {
				e1 = encryptTF(e1, list[i])
				if i == 32 {
					break
				}
			}
			if q == passestodo {
				break
			}
		}
	}
	ioutil.WriteFile(outname, []byte(e1), 0664)
}

func help() {
	slowly(`Ignore3 `+version+` file encryption
    --operation            encrypt/decrypt
    --method               twofish/aes or both(default: both)
    --keygen               keygen: generates 1024 byte key
                                   and saves it as key.pem
    --barcode              (optional)generates barcode key
                                     file in png(pdf417) part
                                     of keygen
    --input                file to encrypt/decrypt
    --output               filename of output
    --key                  1024 byte key file
    --passes               (optional)how many times to perform
                                      the operation(default 1)
    --verbose              verbose output
   https://bitbucket.org/ul7r41337h4x0r/ignore3
`, delay)
	os.Exit(2)
}

func genkey() {
	ioutil.WriteFile("key.pem", []byte(keygen(1024, true, true, true, true, false)), 0664)
}

func main() {
	verbose = flag.Bool("verbose", false, "verbose output")
	passesarg := flag.Int("passes", 1, "how many times to encrypt file")
	keygenarg := flag.Bool("keygen", false, "bool: generates a key file")
	pdf417arg := flag.Bool("barcode", false, "bool: generates barcode a key file in png(pdf417)\npart of keygen")
	flag.StringVar(&method, "method", "both", "twofish/aes or both")
	flag.StringVar(&operationarg, "operation", "", "operation do run {encrypt/decrypt}")
	flag.StringVar(&inputarg, "input", "", "input file to {encrypt/decrypt}")
	flag.StringVar(&outputarg, "output", "", "name of output file")
	flag.StringVar(&keyarg, "key", "", "key file")
	flag.Parse()
	if *keygenarg {
		genkey()
		if *pdf417arg {
			if *verbose {
				fmt.Println("barcode")
			}
			savebarcode()
		}
		os.Exit(0)
	}
	passestodo = *passesarg
	if inputarg == "" {
		slowly("input file name is required\n", delay)
		help()
	}
	file = inputarg
	if outputarg == "" {
		slowly("output file name is required\n", delay)
		help()
	}
	outname = outputarg
	if keyarg == "" {
		slowly("key file is required\n", delay)
		help()
	}
	pemfile = keyarg
	if operationarg == "" {
		slowly("operation value is required\n", delay)
		help()
	}
	if operationarg == "encrypt" {
		enfile(file, method) // encrypt
	}
	if operationarg == "decrypt" {
		defile(file, method) // decrypt
	}
}
