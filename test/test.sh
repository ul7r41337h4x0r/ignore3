#!/bin/bash
testfile=main.go
built=ignore3
rounds=100
echo 'hello world' > infile
go build -o $built ../$testfile
./$built --keygen
./$built --operation encrypt --method both --input infile --output BOTH --key key.pem --passes $rounds --verbose
./$built --operation decrypt --method both --input BOTH --output outputBOTH --key key.pem --passes $rounds --verbose
rm BOTH
testoutputBOTH=$(cat outputBOTH)
rm outputBOTH
if [ "$testoutputBOTH" = "hello world" ]
then
    echo 'AES/Twofish TEST PASSED'
    echo "$testoutputBOTH"
else
    echo 'AES/Twofish failed'
fi
./$built --operation encrypt --method aes --input infile --output AES --key key.pem --passes $rounds --verbose
./$built --operation decrypt --method aes --input AES --output outputAES --key key.pem --passes $rounds --verbose
rm AES
testoutputAES=$(cat outputAES)
rm outputAES
if [ "$testoutputAES" = "hello world" ]
then
    echo 'AES TEST PASSED'
    echo "$testoutputAES"
else
    echo 'AES failed'
fi
./$built --operation encrypt --method twofish --input infile --output TF --key key.pem --passes $rounds --verbose
./$built --operation decrypt --method twofish --input TF --output outputTF --key key.pem --passes $rounds --verbose
rm TF
testoutputTF=$(cat outputTF)
rm outputTF
if [ "$testoutputTF" = "hello world" ]
then
    echo 'TF TEST PASSED'
    echo "$testoutputTF"
else
    echo 'TF failed'
fi
rm key.pem
rm $built
rm infile
